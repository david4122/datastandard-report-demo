package com.stibo.demo.report.service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.stibo.demo.report.logging.LogTime;
import com.stibo.demo.report.model.Attribute;
import com.stibo.demo.report.model.AttributeGroup;
import com.stibo.demo.report.model.AttributeLink;
import com.stibo.demo.report.model.Category;
import com.stibo.demo.report.model.Datastandard;

@Service
public class ReportService {

	private static final String[] HEADERS = {"Category Name", "Attribute Name", "Description", "Type", "Groups"};

	// @LogTime
	public Stream<Stream<String>> report(Datastandard datastandard, String categoryId) {

		var categoryIndex = buildIndex(datastandard.categories(), Category::id);
		var attrIndex = buildIndex(datastandard.attributes(), Attribute::id);
		var grpIndex = buildIndex(datastandard.attributeGroups(), AttributeGroup::id);

		Stream<Stream<String>> headers = Stream.of(Stream.of(HEADERS));
		Stream<Stream<String>> rows = streamParents(categoryIndex, categoryId)
			.flatMap(cat -> cat.attributeLinks().stream()
					.map(attrLink -> buildReportRow(attrIndex, grpIndex, cat, attrLink)));

		return Stream.concat(headers, rows);
	}

	private Stream<Category> streamParents(Map<String, Category> categoryIndex, String startCatId) {
		LinkedList<Category> parents = new LinkedList<>();

		String parentId = startCatId;
		while(null != parentId) {
			Category next = categoryIndex.get(parentId);
			parents.push(next);

			parentId = next.parentId();
		}

		return parents.stream();
	}

	private <K, V> Map<K, V> buildIndex(List<V> ls, Function<V, K> keyGetter) {
		return ls.stream().collect(
				HashMap<K, V>::new,
				(acc, n) -> acc.put(keyGetter.apply(n), n),
				Map::putAll);
	}

	private Stream<String> buildReportRow(
			Map<String, Attribute> attributeIndex,
			Map<String, AttributeGroup> groupIndex,
			Category cat,
			AttributeLink attrLink) {

		Attribute attr = attributeIndex.get(attrLink.id());

		String groupsString = attr.groupIds()
			.stream()
			.map(gid -> groupIndex.get(gid))
			.map(g -> g.name())
			.collect(Collectors.joining("\n"));

		return Stream.of(
				cat.name(),
				attrName(attrLink, attr),
				attr.description(),
				typeToString(attributeIndex, attrLink, attr),
				groupsString);
	}

	private String attrName(AttributeLink link, Attribute attr) {
		return link.optional() ? attr.name() : attr.name() + "*";
	}

	private String typeToString(Map<String, Attribute> attributeIndex, AttributeLink link, Attribute attr) {
		return typeToString(new StringBuilder(), attributeIndex, link, attr, 0).toString();
	}

	private StringBuilder typeToString(
			StringBuilder result,
			Map<String, Attribute> attributeIndex,
			AttributeLink link,
			Attribute attr,
			int depth) {

		result.append(attr.type().id());

		if(null != attr.attributeLinks()) {
			result.append("{\n");

			for(AttributeLink childLink: attr.attributeLinks()) {

				Attribute childAttr = attributeIndex.get(childLink.id());

				result
					.append(indent(depth + 1))
					.append(attrName(childLink, childAttr))
					.append(": ");

				typeToString(result, attributeIndex, childLink, childAttr, depth + 1);

				result.append("\n");
			}

			result
				.append(indent(depth))
				.append("}");
		}

		if(attr.type().multiValue())
			result.append("[]");

		return result;
	}

	private String indent(int depth) {
		return " ".repeat(depth * 2);
	}
}
