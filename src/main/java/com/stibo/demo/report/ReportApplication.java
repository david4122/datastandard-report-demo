package com.stibo.demo.report;

import static java.util.stream.Collectors.joining;
import static org.apache.logging.log4j.util.Strings.dquote;

import java.util.Objects;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stibo.demo.report.model.Category;
import com.stibo.demo.report.service.ReportService;
import com.stibo.demo.report.util.RandomDataGenerator;

@SpringBootApplication
public class ReportApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReportApplication.class, args);
	}

	@Bean
	public CommandLineRunner testAndQuit(ApplicationContext ctx, ObjectMapper mapper, ReportService reportService, RandomDataGenerator rand) {
		return args -> {
			long[] recs = new long[10];

			System.out.println("Running tests...");

			for(int run = 0; run < 10; run++) {
				var ds = rand.nextDatastandard();

				long start = System.currentTimeMillis();
				for(Category cat: ds.categories()) {
					reportService.report(ds, cat.id())
						.map(row -> row.map(c -> escape(c)).collect(joining(","))).collect(joining("\n"));
				}
				long stop = System.currentTimeMillis();

				recs[run] = stop - start;
				System.out.printf(">>> RUN %d: %dms\n", run, recs[run]);
			}

			System.out.printf("avg: %f +/- %f\n", avg(recs), stddev(recs));

			System.out.println("Tests completed. Quitting...");
			((ConfigurableApplicationContext) ctx).close();
		};
	}

	private double stddev(long[] arr) {
		double avg = avg(arr);
		double diffSum = 0d;
		for(long i: arr)
			diffSum += (avg - i) * (avg - i);
		return Math.sqrt(diffSum / arr.length);
	}

	private double avg(long[] arr) {
		double sum = 0d;
		for(long i: arr)
			sum += i;
		return sum / arr.length;
	}

	private static String escape(String cell) {
		if (Objects.isNull(cell)) {
			return "";
		} else if (cell.contains("\"")) {
			return dquote(cell.replace("\"", "\"\""));
		} else if (cell.contains(",") || cell.contains("\n")) {
			return dquote(cell);
		} else {
			return cell;
		}
	}
}
