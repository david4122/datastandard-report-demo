package com.stibo.demo.report.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

import com.stibo.demo.report.model.Attribute;
import com.stibo.demo.report.model.AttributeGroup;
import com.stibo.demo.report.model.AttributeLink;
import com.stibo.demo.report.model.AttributeType;
import com.stibo.demo.report.model.Category;
import com.stibo.demo.report.model.Datastandard;

@Component
public class RandomDataGenerator {

	private static Random rand = new Random();

	private int groups;
	private int attributes;
	private int categories;
	private int attrsPerCategory;
	private double attrNoChildrenChance;
	private int attrMaxChildren;

	public RandomDataGenerator() {
		this(1_000, 10_000, 10_000, 500, 0.8, 10);
	}

	public RandomDataGenerator(int groups, int attributes, int categories, int attrsPerCategory, double attrNoChildrenChance, int attrMaxChildren) {
		this.groups = groups;
		this.attributes = attributes;
		this.categories = categories;
		this.attrsPerCategory = attrsPerCategory;
		this.attrNoChildrenChance = attrNoChildrenChance;
		this.attrMaxChildren = attrMaxChildren;
	}

	public Datastandard nextDatastandard() {
		var groups = IntStream.range(0, this.groups).mapToObj(i -> nextGroup()).toList();
		var groupIds = groups.stream().map(g -> g.id()).toList();

		var attrs = new ArrayList<Attribute>(this.attributes);
		for(int i = 0; i < this.attributes; i++)
			attrs.add(nextAttribute(attrs, groupIds));

		var cats = new ArrayList<Category>(this.categories);
		for(int i = 0; i < this.categories; i++)
			cats.add(nextCategory(attrs, cats));

		return new Datastandard(randStr(), randStr(), cats, attrs, groups);
	}

	public Category nextCategory(List<Attribute> attrs, List<Category> cats) {
		var randAttrs = getRandom(attrs, this.attrsPerCategory)
			.map(attr -> new AttributeLink(attr.id(), rand.nextBoolean()))
			.toList();

		var parentId = cats.size() == 0 ? null : cats.get(rand.nextInt(cats.size())).id();

		return new Category(
				randStr(),
				randStr(),
				randStr(),
				parentId,
				randAttrs);
	}

	public Attribute nextAttribute(List<Attribute> attrs, List<String> groups) {
		var randAttrs = rand.nextFloat() <= this.attrNoChildrenChance ? null : getRandom(attrs, rand.nextInt(this.attrMaxChildren))
			.map(attr -> new AttributeLink(attr.id(), rand.nextBoolean()))
			.toList();

		var randGroups = getRandom(groups, 3).toList();

		return new Attribute(
				randStr(),
				randStr(),
				randStr(),
				new AttributeType(randStr(), rand.nextBoolean()),
				randAttrs,
				randGroups);
	}

	public AttributeGroup nextGroup() {
		return new AttributeGroup(randStr(), randStr(), randStr());
	}

	public String randStr() {
		return UUID.randomUUID().toString();
	}

	private <T> Stream<T> getRandom(List<T> src, int len) {
		return src.size() < 5 ? Stream.<T>of() : IntStream.range(0, len)
			.map(i -> rand.nextInt(src.size()))
			.mapToObj(i -> src.get(i));
	}
}
